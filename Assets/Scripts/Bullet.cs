﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Bullet : MonoBehaviour
    {
        public float Speed = 1;
        public Vector3 Direction;

        void SetDir(Vector3 dir)
        {
            Direction = dir;
        }

        void FixedUpdate()
        {
            transform.position += Direction * Time.deltaTime * Speed;
        }

        void OnBecameInvisible()
        {
            Destroy(gameObject);
        }

        void OnTriggerEnter2D(Collider2D coll)
        {

            if (coll.gameObject.CompareTag("ennemy"))
                coll.SendMessage("TakeDamage");
            if (coll.gameObject.CompareTag("wall") || coll.gameObject.CompareTag("ennemy"))
                Destroy(gameObject);
        }
    }
}
