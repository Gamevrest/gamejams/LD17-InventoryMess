﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    public float Speed = 1;
    public float Cooldown = 10;
    public GameObject Bullet;

    private Camera _c;
    private LineRenderer _sight;
    private Animator _animator;
    private SpriteRenderer _renderer;
    private Vector3 _mousePos;
    private GameObject _bulletFolder;
    private float _timer = 0;
	private Rigidbody2D _rig;

    void Main()
    {
		_rig = GetComponent<Rigidbody2D> ();
        _c = Camera.main;
        _sight = GetComponent<LineRenderer>();
        _animator = GetComponent<Animator>();
        _animator.SetInteger("direction", 1);
        _renderer = GetComponent<SpriteRenderer>();
        _bulletFolder = new GameObject("BULLETS");
    }

    void FixedUpdate()
    {
		if (_rig.velocity != Vector2.zero)
			_rig.velocity = Vector2.zero;
        transform.Translate(Time.deltaTime * new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0) * Speed);
    }

    void Update()
    {
        _mousePos = _c.ScreenToWorldPoint(Input.mousePosition);
        _mousePos.z = 0;
        _timer--;

        _sight.SetPositions(new[] { transform.position, _mousePos });
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D)) _animator.SetBool("moving", true);
        if (Input.GetKeyUp(KeyCode.Q) || Input.GetKeyUp(KeyCode.Z) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D)) _animator.SetBool("moving", false);
        if (Input.GetKey(KeyCode.Z)) _animator.SetInteger("direction", 1);
        else if (Input.GetKey(KeyCode.S)) _animator.SetInteger("direction", -1);
        else if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.D)) _animator.SetInteger("direction", 0);
        if (Input.GetKey(KeyCode.Q)) _renderer.flipX = true;
        else if (Input.GetKey(KeyCode.D)) _renderer.flipX = false;
        if (_timer <= 0 && Input.GetMouseButton(0) && !GetComponent<inventory>()._inventory.isOpen)
        {
            var tmp = Instantiate(Bullet, transform.position, Quaternion.identity, _bulletFolder.transform);
            var dir = (Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position)).normalized;
            tmp.transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg, Vector3.forward);
            tmp.SendMessage("SetDir", dir);
            _timer = Cooldown;
        }
    }

    void OnCollisionEnter2D(Collision2D o)
    {
        if (o.transform.CompareTag("weapon"))   
            if (GetComponent<inventory>()._inventory.AddItem(o.transform.GetComponent<InventoryItem>()))
                Destroy(o.transform.gameObject);
    }
}
