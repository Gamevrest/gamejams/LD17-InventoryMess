﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragHandeler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public GameObject empty;
	public static GameObject itemBeingDragged;
	Vector3 startPosition;
	Transform startParent;
	bool isDragable;

	void changeDragable(bool b){
		isDragable = b;
	}

	void Start(){
		isDragable = gameObject.transform.name != "empty";
	}

	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		if (!isDragable)
			return;
		itemBeingDragged = gameObject;
		startPosition = transform.position;
		startParent = transform.parent;
		GetComponent<CanvasGroup> ().blocksRaycasts = false;
	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		if (!isDragable)
			return;
		transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		if (!isDragable)
			return;
		itemBeingDragged = null;
		if (eventData.pointerCurrentRaycast.gameObject == null || eventData.pointerCurrentRaycast.gameObject.transform.parent.name == "Drop") {
			//print (eventData.pointerCurrentRaycast.gameObject.name);
			transform.SetParent (null);
			var tmp = Instantiate (empty);
			tmp.transform.name = "empty";
			tmp.SendMessage ("changeDragable", false);
			tmp.transform.SetParent (startParent);
			//Destroy (gameObject,1f);
		}else {
			GetComponent<CanvasGroup> ().blocksRaycasts = true;
			if (transform.parent == startParent)
				transform.position = startPosition;
		}
	}

	#endregion
}
