using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class Drop : MonoBehaviour, IDropHandler {
	public GameObject item {
		get{
			if (transform.childCount > 0) {
				return transform.GetChild (0).gameObject;
			}
			return null;
		}
	}
	public GameObject empty;

	void Start(){
		//print (transform.name);
		//print (transform.childCount);
	}

	#region IDropHandler implementation

	public void OnDrop (PointerEventData eventData)
	{
		if (eventData.dragging && DragHandeler.itemBeingDragged) {
			print (transform.name);
			if (!item) {
				DragHandeler.itemBeingDragged.transform.SetParent (transform);
			} else {
				var tmp = Instantiate (empty);
				tmp.transform.SetParent (DragHandeler.itemBeingDragged.transform.parent);
				Destroy(DragHandeler.itemBeingDragged, 1f);
			}
		}
	}

	#endregion
}
