﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class GameHandler : MonoBehaviour
{
    private GameObject _itemFolder;

    void Start()
    {
        _itemFolder = new GameObject("ITEMS");
    }

    public void DropObject(Message m)
    {
        if (m.Target.name == "player")
            DropPlayer(m.Target, m.List.First(), _itemFolder.transform);
        var i = 0f;
        foreach (GameObject g in m.List)
            StartCoroutine(Spawn(g, m.Target.position, i += 0.1f, _itemFolder.transform));
    }

    IEnumerator Spawn(GameObject g, Vector3 pos, float time, Transform parent)
    {
        yield return new WaitForSeconds(time);
        var tmp = Instantiate(g, pos, Quaternion.identity, parent);
        tmp.GetComponent<InventoryItem>().initialiseItem(new Weapon(1, element.Fire, "caca"));
        tmp.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1, 1), Random.Range(-1, 1)) * Random.Range(1, 5), ForceMode2D.Impulse);
    }

    void DropPlayer(Transform t, GameObject o, Transform p)
    {
        var tmp = Instantiate(o, t.position + Vector3.right, Quaternion.identity, p);
        tmp.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 5, ForceMode2D.Impulse);
    }
}

public class Message
{
    public Transform Target;
    public GameObject[] List;

    public Message(Transform target, GameObject[] list)
    {
        Target = target;
        List = list;
    }
}

public class nameGenerator {
	string[] firstName = new string[]{
		"Surprising",
		"Mysterious",
		"Marvelous",
		"Polished",
		"Eternal",
		"Cheap",
		"Dry",
		"Disgusting",
		"Exotic",
		"Usefull",
		"Tasty",
		"Random",
		"Standard",
		"Beautiful",
		"Deadly",
		"Ugly",
		"Fantastic",
		"Golden",
		"Fluffy",
		"Sweet",
		"Useless"};
	string[] armorName = new string[]{
		"protection",
		"metal piece",
		"armor",
		"shield",
		"gear",
		"plate",
		"tank",
		"ballabsorber",
		"defense",
		"shelter",
		"pan",
		"leaf",
		"carton",
		"@\\#!:~@-",
		"shell",
		"cloth",
		"wool",
		"plastic"};
	string[] wpName = new string[]{
		"flower",
		"gun",
		"piece of shit",
		"crap",
		"weapon",
		"baguette",
		"thing",
		"ballthrower",
		"bananagun",
		"digital axe",
		"umbrella",
		"laser",
		"sponge",
		"speaker",
		"wood plank",
		"canon",
		"minigun",
		"bread slice",
		"blade"};

	string generateWpName() {
		return (firstName[Random.Range(0, firstName.Length)] + " " + wpName[Random.Range(0, wpName.Length)]);
	}

	string generateArmorName() {
		return (firstName[Random.Range(0, firstName.Length)] + " " + armorName[Random.Range(0, armorName.Length)]);
	}
}