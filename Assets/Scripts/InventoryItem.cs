﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem : MonoBehaviour {

	public Sprite spriteNeutral;

	public Item item;

	public int maxSize;

	public void initialiseItem(Item i){
		item = i;
	}
	public string getText(){
		switch (item.GetType().ToString()){
		case "Weapon":
			var tmp = item as Weapon;
			return item.getName()+"\n"+tmp.getDamage()+" dmg";
		default:
			return "";
		}

	}
}
