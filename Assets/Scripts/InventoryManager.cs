﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;


public class InventoryManager : MonoBehaviour {
	Vector3 normalScale = new Vector3(0.75f,0.75f,0);
	Vector3 minimisedScale = new Vector3(0,0,0);
	public float speed = 0.1f;

	public GameObject tooltipObject;
	private static GameObject tooltip;
	public Text sizeTextObject;
	private static Text sizeText;
	public Text visualTextObject;
	private static Text visualText;

	public GameObject Case;
	public GameObject ItemSlot;

	private List<GameObject> allSlots = new List<GameObject>();

	private List<GameObject> EquipedItems = new List<GameObject>();

	private static Slot from, to;

	public int emptySlot;

	bool locked = false;

	public GameObject iconPrefab;

	public Canvas canvas;
	public EventSystem eventSystem;

	private float hoverYOffset;
	private float hoverXOffset;

	private static GameObject hoverObject;

	public bool isOpen {
		get { return !locked && transform.localScale.x > 0.1f && transform.localScale.y > 0.1f; }
	}

	// Use this for initialization
	void Start () {
		tooltip = tooltipObject;
		sizeText = sizeTextObject;
		visualText = visualTextObject;
		from = to = null;
		CreateInventory ();
	}

	void Update(){
		if (Input.GetMouseButtonUp (0)) {
			if (!eventSystem.IsPointerOverGameObject (-1) && from != null) {
				from.GetComponent<Image> ().color = Color.white;
				from.ClearSlot ();
				Destroy(GameObject.Find("Hover"));
				to = null;
				from = null;
				hoverObject = null;
			}
		}

		if (hoverObject != null) {
			Vector2 position;
			RectTransformUtility.ScreenPointToLocalPointInRectangle (canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out position);
			position.Set (position.x+hoverXOffset, position.y - hoverYOffset);
			hoverObject.transform.position = canvas.transform.TransformPoint (position);
		}
	}

	public void ShowTooltip(GameObject slot){
		Slot tmpSlot = slot.GetComponent<Slot>();

		if (!tmpSlot.isEmpty && hoverObject == null) {
			tooltip.SetActive (true);

			
			float xPos = slot.transform.position.x - 0.12f;
			float yPos = slot.transform.position.y - 0.42f;

			tooltip.transform.position = new Vector2 (xPos, yPos);

			changeText (tmpSlot.CurrentItem.getText ());
		}
	}

	public void HideTooltip(){
		tooltip.SetActive (false);
	}

	void CreateInventory() {
		GameObject tmp = null;
		for(int i = 0; i < 9; i++){
			for (int j = 0; j < 11; j++) {
				tmp = Instantiate (Case, new Vector3 (0, 0, 0), Quaternion.identity);
				tmp.transform.name = i+"x"+j;
				tmp.transform.SetParent(ItemSlot.transform);
				tmp.transform.localScale = new Vector3 (1, 1, 1);
				allSlots.Add (tmp);
			}
		}
		hoverYOffset = tmp.GetComponent<RectTransform>().sizeDelta.y*0.125f;
		hoverXOffset = tmp.GetComponent<RectTransform>().sizeDelta.x*0.125f;
		emptySlot = allSlots.Count;
		tmp = transform.Find ("EquipedItem").gameObject;
		foreach (Transform c in tmp.transform) {
			EquipedItems.Add (c.gameObject);
		}
	}

	public bool AddItem(InventoryItem item){
		if (item.maxSize == 1) {
			return PlaceEmpty (item);
		} else {
			foreach (GameObject slot in allSlots) {
				Slot tmp = slot.GetComponent<Slot> ();

				if (!tmp.isEmpty) {
					if (tmp.CurrentItem.GetType() == item.GetType() && tmp.isAvailable) {
						tmp.AddItem (item);
						return true;
					}
				}
			}
			if (emptySlot > 0) {
				PlaceEmpty (item);
			}
		}
		return false;
	}

	private bool PlaceEmpty(InventoryItem item){
		if (emptySlot > 0) {
			foreach (GameObject slot in allSlots) {
				Slot tmp = slot.GetComponent<Slot> ();
				if (tmp.isEmpty) {
					tmp.AddItem (item);
					emptySlot--;
					return true;
				}
			}
		}
		return false;
	}

	public void MoveItem(GameObject clicked){
		try{
			if (from == null) {
				if (!clicked.GetComponent<Slot> ().isEmpty) {
					from = clicked.GetComponent<Slot> ();
					from.GetComponent<Image> ().color = Color.gray;
					
					hoverObject = Instantiate(iconPrefab);
					hoverObject.GetComponent<Image>().sprite = clicked.GetComponent<Image>().sprite;
					hoverObject.name = "Hover";

					RectTransform hoverTransform = hoverObject.GetComponent<RectTransform>();
					RectTransform clieckedTransform = clicked.GetComponent<RectTransform>();

					hoverTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, clieckedTransform.sizeDelta.x);
					hoverTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, clieckedTransform.sizeDelta.y);

					hoverObject.transform.SetParent(GameObject.Find("Canvas").transform, true);
					hoverObject.transform.localScale = from.gameObject.transform.localScale;
				} 
			} else if (to == null) {
				to = clicked.GetComponent<Slot> ();
				Destroy(GameObject.Find("Hover"));
			}
			if (to != null && from != null) {
				Stack<InventoryItem> tmpTo = new Stack<InventoryItem> (to.Items);
				to.AddItems (from.Items);
				if (tmpTo.Count == 0) {
					from.ClearSlot();
				} else {
					from.AddItems (tmpTo);
				}

				from.GetComponent<Image> ().color = Color.white;
				to = null;
				from = null;
				hoverObject = null;
			}
		} catch (Exception e){
			print (e);
			if (from)
				from.GetComponent<Image> ().color = Color.white;
			to = null;
			from = null;
			hoverObject = null;
		}
	}

	void changeText(string text){
		sizeText.text = text;
		visualText.text = text;
	}







	IEnumerator openGUI(){
		if (locked)
			yield break;
		locked = true;
		Vector3 actual = transform.localScale;
		for (float t = 0; t < 1.25f; t += Time.deltaTime / speed) {
			transform.localScale = Vector3.Lerp (actual, normalScale, t);
			yield return null;
		}
		locked = false;
	}

	IEnumerator closeGUI(){
		if (locked)
			yield break;
		locked = true;
		Vector3 actual = transform.localScale;
		for (float t = 0; t < 1.25f; t += Time.deltaTime / speed) {
			transform.localScale = Vector3.Lerp (actual, minimisedScale, t);
			yield return null;
		}
		locked = false;
	}

	public void CloseGUI(){
	//	print ("close");
		StartCoroutine ("closeGUI");
	}

	public void OpenGUI(){
//		print ("open");
		StartCoroutine ("openGUI");
	}
}
