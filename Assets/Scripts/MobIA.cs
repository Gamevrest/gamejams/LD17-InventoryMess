﻿using UnityEngine;
using System.Collections;

public class MobIA : MonoBehaviour
{
    public float Speed = 5;
    public Transform Player;
    public int Life = 5;
    public GameObject TestWeapon;

    private Vector3 _lastpos;
    private Animator _anim;

    void Start()
    {
        if (!Player) Player = GameObject.FindGameObjectWithTag("Player").transform;
        _anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        transform.Translate((Player.position - transform.position).normalized * Time.deltaTime * Speed);
    }

    void Update()
    {
        _anim.SetInteger("direction", -(int)Mathf.Sign(_lastpos.y - transform.position.y));
        _lastpos = transform.position;
        if (Life <= 0)
        {
            Camera.main.SendMessage("DropObject", new Message(transform, new[]
            {
                TestWeapon, TestWeapon, TestWeapon, TestWeapon, TestWeapon
            }));
            Destroy(gameObject);
        }
    }

    void TakeDamage()
    {
        Life--;
    }
}
