using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Slot : MonoBehaviour {
	private Stack<InventoryItem> items;

	public Stack<InventoryItem> Items{
		get { return items; }
		set	{ items = value; }
	}

	public Sprite slotEmpty;

	public bool isEmpty
	{
		get { return items.Count == 0; }
	}

	public bool isAvailable {
		get { return CurrentItem.maxSize > items.Count;}
	}

	public InventoryItem CurrentItem{
		get { return items.Peek (); }
	}

	void Start(){
		items = new Stack<InventoryItem> (); 
	}

	public void AddItem(InventoryItem item){
		items.Push (item);

		ChangeSprite (item.spriteNeutral);
	}

	public void AddItems(Stack<InventoryItem> items){
		this.items = new Stack<InventoryItem> (items);

		ChangeSprite (CurrentItem.spriteNeutral);
	}

	private void ChangeSprite(Sprite neutral){
		GetComponent<Image> ().sprite = neutral;
	}

	public void ClearSlot(){
		items.Clear();
		ChangeSprite (slotEmpty);
	}
		
}
