﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inventory : MonoBehaviour {

	public InventoryManager _inventory;
	// Use this for initialization
	void Start () {
		_inventory = GameObject.FindGameObjectWithTag ("Inventory").GetComponent<InventoryManager>();
	}
	
	void Update () {
		if (Input.GetKeyUp(KeyCode.E)){
			if (_inventory.isOpen ){
				_inventory.CloseGUI();
			} else {
				_inventory.OpenGUI();
			}
		}
	}
}
