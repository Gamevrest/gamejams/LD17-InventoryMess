﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum element {Neutral, Wind, Water, Fire, Earth}


public class Item
{
	element _type;
	string _name;

	public Item()
	{
		_type = element.Neutral;
		_name = "Unknown thing";
	}

	public Item(element type, string name)
	{
		_type = type;
		_name = name;
	}

	public element getType(){
		return _type;
	}

	public string getName(){
		return _name;
	}
}

public class Weapon : Item
{
	int _dmg;

	public Weapon()
	{
		_dmg = 1;
	}

	public Weapon(int dmg, element type, string name) : base(type, name)
	{
		_dmg = dmg;
	}

	public int getDamage(){
		return _dmg;
	}
}